//
//  CustomProjectTableViewCell.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/18/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import UIKit

class CustomProjectTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        projectAndMarkLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBOutlet weak var projectAndMarkLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var projectMarkLabel: UILabel!
}
