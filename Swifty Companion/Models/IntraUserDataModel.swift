//
//  IntraStudentDataModel.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/12/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import Foundation
import UIKit

//_____________main data model to get user info_____________\\
struct User: Decodable {
    let displayName: String?
    let login: String?
    let imageUrl: String?
    let location: String?
    let email: String?
    let phone: String?
    let level: String?
    let correctionPoints: Int?
    let wallet: Int?
    let grade: String?
    var projectsUsers: [Project]?
    let cursusUsers: [Cursus]?
    var coalitionInfo: Coalition?
    private enum CodingKeys : String, CodingKey {
        case displayName = "displayname", login, imageUrl = "image_url", location, email, phone, level, correctionPoints = "correction_point", wallet, grade, projectsUsers = "projects_users", cursusUsers = "cursus_users", coalitionInfo
    }
}

struct Project: Decodable {
    var name: String?
    let finalMark: Int?
    let status: String?
    let validated: Int?
    var project: ProjectInfo?
    let cursusIDS: [Int]?

    private enum CodingKeys : String, CodingKey {
        case name, finalMark = "final_mark", status, validated = "validated?", project, cursusIDS = "cursus_ids"
    }
}

struct ProjectInfo: Decodable {
    let name: String?
}

struct Cursus: Decodable {
    let cursusID: Int?
    let grade: String?
    let level: Double?
    let skills: [Skill]?
    private enum CodingKeys : String, CodingKey {
        case cursusID = "cursus_id", grade, level, skills
    }
}

//_____________data model to get user coalition info_____________\\

struct Coalition: Decodable {
    let id: Int?
    let name: String?
    let imageUrl: String?
    let score: Int?
    private enum CodingKeys : String, CodingKey {
        case id, name, imageUrl = "image_url", score
        
    }
}

//_____________data model to get skills info_____________\\

struct Skill: Decodable {
    let level: Double?
    let name: String?
}
