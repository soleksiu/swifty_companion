//
//  CustomSkillTableViewCell.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/18/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import UIKit

class CustomSkillTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        skillTitleLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBOutlet weak var skillTitleLabel: UILabel!
    @IBOutlet weak var skillProgressBar: UIProgressView!
    @IBOutlet weak var skillLevelLabel: UILabel!
}
