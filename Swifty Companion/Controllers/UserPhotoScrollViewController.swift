//
//  UserPhotoScrollViewController.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/17/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import UIKit

class UserPhotoScrollViewController: UIViewController, UIScrollViewDelegate {
    var userPhoto: UIImage?
    var userLogin: String?
    var userName: String?
    var userLocation: String?
    var scrollViewZoom: CGFloat?
    var myImageView: UIImageView?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = userName
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "school_42_background")!)
        self.userLocationLabel.text = userLocation
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.myImageView = UIImageView(image: userPhoto)
        myImageView?.layer.masksToBounds = true
        myImageView?.layer.cornerRadius = 21
        self.userPhotoScrollView.addSubview(myImageView!)
        self.userPhotoScrollView.maximumZoomScale = 1.8
        DispatchQueue.main.async() {
            self.setScrollViewZoom(size: (self.userPhoto?.size)!)
        }
    }
    
    
    func setScrollViewZoom(size: CGSize) {
        let zoomFactorWidth = self.userPhotoScrollView.bounds.size.width/(self.userPhoto?.size.width)!
        let zoomFactorHeight = self.userPhotoScrollView.bounds.size.height/(self.userPhoto?.size.height)!
        self.scrollViewZoom = min(zoomFactorWidth, zoomFactorHeight)
        self.userPhotoScrollView.minimumZoomScale = self.scrollViewZoom!
        self.userPhotoScrollView.zoomScale = self.scrollViewZoom!
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let offsetX = max((scrollView.bounds.width - scrollView.contentSize.width) * 0.5, 0)
        let offsetY = CGFloat(0)
        scrollView.contentInset = UIEdgeInsetsMake(offsetY, offsetX, 0, 0)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return myImageView
    }
    override func viewWillTransition( to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator ) {
        DispatchQueue.main.async() {
            self.setScrollViewZoom(size: (self.userPhoto?.size)!)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBOutlet weak var userPhotoScrollView: UIScrollView!
    @IBOutlet weak var userLocationLabel: UILabel!
}
