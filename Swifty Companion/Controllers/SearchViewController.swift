//
//  SearchViewController.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/2/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITextFieldDelegate, APIIntraDelegate {
    
    var accessToken : String?
    var delegate: APIIntraController?
    var curUser : User?
    var segueCanBePerformed: Bool?
    var window: UIWindow?
    var errorWithRequest: Bool?
    var errorCode: String?

    func receiveUserInfo(info: User) {
        self.curUser = info
    }
    
    
    func makeRequestsToApi(completion: @escaping () -> ()) {
        let group = DispatchGroup()
        group.enter()
        self.delegate?.makeIntraRequest(indicator: loadIndicator, requestBody: self.searchField.text!.lowercased()){
            group.leave()
        }
        group.wait()
        
        let group2 = DispatchGroup()
        group2.enter()
        self.delegate?.getCoalitionInfo(requestBody: self.searchField.text!.lowercased()) {
            group2.leave()
        }
        group2.wait()
        completion()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        segueCanBePerformed = requestIsValid()
        if segueCanBePerformed == true {
            let group = DispatchGroup()
            group.enter()
            DispatchQueue.main.async {
                self.loadIndicator.isHidden = false
                self.loadIndicator.startAnimating()
            }
            makeRequestsToApi() {
                DispatchQueue.main.async {
                    self.loadIndicator.stopAnimating()
                    self.loadIndicator.isHidden = true
                }
                group.leave()
            }
            group.wait()
            segueCanBePerformed = requestIsValid()
            if segueCanBePerformed == true {
                performSegue(withIdentifier: "showResults", sender: self)
            }
        }
        return true
    }
    @IBOutlet weak var searchButtonSettings: UIButton!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchField: UITextField!
    @IBAction func searchButton(_ sender: UIButton) {
        segueCanBePerformed = requestIsValid()
        if segueCanBePerformed == true {
            let group = DispatchGroup()
            group.enter()
            DispatchQueue.main.async {
                self.loadIndicator.isHidden = false
                self.loadIndicator.startAnimating()
            }
            makeRequestsToApi() {
                DispatchQueue.main.async {
                    self.loadIndicator.stopAnimating()
                    self.loadIndicator.isHidden = true
                }
                group.leave()
            }
            group.wait()
            segueCanBePerformed = requestIsValid()
            if segueCanBePerformed == true {
                performSegue(withIdentifier: "showResults", sender: self)
            }
        }
    }
    
    func errorHandler(error: NSError) {
//        print("Error: \(error)")
//        print("error func")
        errorWithRequest = true
        errorCode = String(describing: error.code)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.errorWithRequest = false
        searchButtonSettings.layer.cornerRadius = 10
        searchField.layer.cornerRadius = 10
        self.searchField.delegate = self
        segueCanBePerformed = true
        loadIndicator.isHidden = true
//        loadIndicator.hidesWhenStopped = true
        getIntraAccessToken()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func showErrorMessage(titleText: String, messageText: String, buttonText: String) {
        let error = UIAlertController(title: titleText, message: messageText, preferredStyle: .alert)
        error.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: nil))
        DispatchQueue.main.async {
            self.present(error, animated: true, completion: nil)
        }
    }
    
    func getIntraAccessToken() {
        let UID = "39f38a02c8abe522462574f5437164106a3e3234b4d2acb87f7d41a137fa121c"
        let SECRET = "81966156dc1710b025018196ffc6ff897a87e1b9af3791bf16b9aa91c0ea1491"
        let BEARER = ((UID + ":" + SECRET).data(using: String.Encoding.utf8, allowLossyConversion: true)?.base64EncodedString(options: NSData.Base64EncodingOptions (rawValue: 0)))
        let url_tokens = "https://api.intra.42.fr/oauth/token"
        let url_k = NSURL(string: url_tokens)!
        let request = NSMutableURLRequest(url: url_k as URL)
        request.httpMethod = "POST"
        request.setValue("Basic " + BEARER!, forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = "grant_type=client_credentials".data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {
            (data, response, error) in
//            print(response ?? "")
            if error != nil {
                print(error ?? "")
            }
            else if let d = data {
                do {
                    if let dic : NSDictionary = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
//                        print(dic)
                        self.accessToken = dic.object(forKey: "access_token") as? String
                        print("\nAccess token: \(self.accessToken ?? "error getting token")\n")

                    }
                }
                catch (let err) {
                    print(err)
                }
                
            }
            self.delegate = APIIntraController(delegate: self, tokenToSet: self.accessToken!)
//            DispatchQueue.main.async {
//                self.delegate?.makeIntraRequest(requestBody: self.searchField.text!)
//            }
        }
        task.resume()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if self.errorWithRequest == false && self.delegate?.userInfo != nil {
            self.curUser = self.delegate?.userInfo
            if let resultView = segue.destination as? ResultsViewController {
                resultView.userToShow = self.curUser
                if self.curUser?.projectsUsers != nil {
                    resultView.projects = (self.curUser?.projectsUsers)!
                }
                if self.curUser?.cursusUsers != nil && self.curUser?.cursusUsers![0].skills != nil {
                    resultView.skills = (self.curUser?.cursusUsers![0].skills)!
                }
            }
        }
    }

    func requestIsValid() -> Bool {
        let requestText = searchField.text
        let oneSpaceString = requestText?.components(separatedBy: " ").filter { !$0.isEmpty }.joined(separator: " ")
        let searchWord = oneSpaceString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //        print(searchWord ?? "None")
        if searchWord?.isEmpty == true {
            showErrorMessage(titleText: "", messageText: "No search words, try again.", buttonText: "Ok")
            return false
        }
        else if errorWithRequest == true {
            if errorCode == "4864" {
                showErrorMessage(titleText: "", messageText: "User not found", buttonText: "Ok")
            }
            else {
                showErrorMessage(titleText: "Error", messageText: errorCode!, buttonText: "Ok")
            }
            errorWithRequest = false
            return false
        }
        
        return true
    }

    override func viewWillAppear(_ animated: Bool) {
        searchField.text = ""
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        DispatchQueue.main.async {
            self.segueCanBePerformed = self.requestIsValid()
        }
        return segueCanBePerformed!
    }
}

