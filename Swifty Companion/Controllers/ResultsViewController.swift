//
//  ResultsViewController.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/2/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import UIKit
import Foundation
import MessageUI

class ResultsViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    var projects : [Project] = []
    var skills : [Skill] = []
    var userToShow : User?
    var imageFromUrl : UIImage?
    let coalBackgrounds = [7:"https://cdn.intra.42.fr/coalition/cover/7/empire_background.jpg", 8:"https://cdn.intra.42.fr/coalition/cover/8/hive_background.jpg", 6:"https://cdn.intra.42.fr/coalition/cover/6/union_background.jpg", 5:"https://cdn.intra.42.fr/coalition/cover/5/alliance_background.jpg"]
    var circleLayerTrack = CAShapeLayer()
    var circleLayer = CAShapeLayer()
    var progressBarEndAngle: CGFloat = CGFloat.pi * 2
    var portraitCornerRadius: CGFloat = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == projectsTableView {
            return projects.count
        }
        else if tableView == skillsTableView {
            return skills.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == projectsTableView {
            let cellProject = tableView.dequeueReusableCell(withIdentifier: "cellProject") as! CustomProjectTableViewCell
            let projectTitle = (projects[indexPath.row].project?.name ?? "Not found")
            var projectMark: String
            if projects[indexPath.row].finalMark == nil {
                projectMark = ""
            }
            else {
                projectMark = "Mark: \(String(describing: projects[indexPath.row].finalMark!))%"
            }
            cellProject.projectAndMarkLabel?.text = projectTitle
            cellProject.projectMarkLabel?.text = projectMark
            if projects[indexPath.row].status != nil && projects[indexPath.row].status! == "finished" {
                cellProject.statusLabel?.text = (projects[indexPath.row].validated != nil && projects[indexPath.row].validated! == 1) ? "Success" : "Fail"
                cellProject.statusLabel?.textColor = (projects[indexPath.row].validated != nil && projects[indexPath.row].validated! == 1) ? UIColor.green : UIColor.red
            }
            else if projects[indexPath.row].status == nil{
                cellProject.statusLabel?.text = ""
            }
            else {
                cellProject.statusLabel?.text = "In progress..."
                cellProject.statusLabel?.textColor = UIColor.cyan
            }
            cellProject.selectionStyle = .none
            return cellProject
        }
        else if tableView == skillsTableView {
            let cellSkill = tableView.dequeueReusableCell(withIdentifier: "cellSkill") as! CustomSkillTableViewCell
            let skillName = (skills[indexPath.row].name ?? "Not found")
            let skillLevel = "\(String(describing: (skills[indexPath.row].level)!))"
            cellSkill.skillTitleLabel?.text = skillName
            cellSkill.skillLevelLabel?.text = skillLevel
            cellSkill.skillProgressBar?.setProgress((Float((skills[indexPath.row].level)! / 21)), animated: false)
            cellSkill.selectionStyle = .none
            return cellSkill
        }
        return UITableViewCell()
    }
    
    func getImageFromUrl(urlText: String, completion: @escaping () -> ()) {
        let imageTask = URLSession.shared.dataTask(with: URL(string: urlText)!) { (imageData, _, imageError) in
            if let imageError = imageError { print(imageError); return }
            self.imageFromUrl = UIImage(data: imageData!)!
                completion()
        }
        imageTask.resume()
    }
    @objc func pressOnImage() {
        performSegue(withIdentifier: "showUserPhoto", sender: self)
    }
    
    @objc func pressOnPhoneNumber() {
        let alert = UIAlertController(title: "Make a call", message: "Are you sure you want to call to \((self.userToShow?.phone)!)?", preferredStyle: .alert)
        let callAction = UIAlertAction(title: "Call", style: .default) { (alert: UIAlertAction!) -> Void in
            let urlString = "telprompt://\(String(describing: (self.userToShow?.phone)!))"
            let urlEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            let url:NSURL = NSURL(string: urlEncoded)!
            UIApplication.shared.open(url as URL)
            print("Call pressed")
        }
        let nopeAction = UIAlertAction(title: "Nope", style: .default) { (alert: UIAlertAction!) -> Void in
            print("Phone cancel pressed")
        }
        alert.addAction(callAction)
        alert.addAction(nopeAction)
        present(alert, animated: true, completion:nil)

    }

    @objc func pressOnEmail() {
        let alert = UIAlertController(title: "Send an email", message: "Are you sure you want to send email to \((self.userToShow?.email)!)?", preferredStyle: .alert)
        let callAction = UIAlertAction(title: "Send", style: .default) { (alert: UIAlertAction!) -> Void in
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["\(String(describing: self.userToShow?.email!))"])
                mail.setMessageBody("<p>Hello from Soleksiu's Swifty Companion</p>", isHTML: true)
                self.present(mail, animated: true)
            } else {
                print("No mail app")
            }
                
            print("Send pressed")
        }
        let nopeAction = UIAlertAction(title: "Nope", style: .default) { (alert: UIAlertAction!) -> Void in
            print("Email cancel pressed")
        }
        alert.addAction(callAction)
        alert.addAction(nopeAction)
        present(alert, animated: true, completion:nil)
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scrollView = segue.destination as? UserPhotoScrollViewController {
            scrollView.userPhoto = self.profilePhoto.image
            scrollView.userLogin = userToShow?.login
            scrollView.userName = self.studentNameLabel.text
            scrollView.userLocation = self.locationLabel.text
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async() {
            let newCirclePath = UIBezierPath(arcCenter: CGPoint(x: self.profilePhoto.center.x, y: self.profilePhoto.center.y), radius: self.profilePhoto.layer.frame.size.width / 2, startAngle: 2.094, endAngle: self.progressBarEndAngle, clockwise: true)
            self.circleLayer.path = newCirclePath.cgPath
            let newTrackPath = UIBezierPath(arcCenter: CGPoint(x: self.profilePhoto.center.x, y: self.profilePhoto.center.y), radius: self.profilePhoto.layer.frame.size.width / 2, startAngle: 2.094, endAngle: 1.047, clockwise: true)
            self.circleLayerTrack.path = newTrackPath.cgPath
            //            if UIDevice.current.orientation.isLandscape == true {
            self.profilePhoto.layer.cornerRadius = self.profilePhoto.layer.frame.size.width / 2
            //
            //            }
            //            else if UIDevice.current.orientation.isPortrait == true {
            //                self.profilePhoto.layer.cornerRadius = self.profilePhoto.layer.frame.size.width / 2
            //            }
        }
        perfomAnimation()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.portraitCornerRadius = (profilePhoto.frame.size.width) / 2 + 5
        self.profilePhoto.layer.masksToBounds = true
        if projects.isEmpty {
            projectsTableView.isHidden = true
            projectsLabel.isHidden = true
        }
        if skills.isEmpty {
            skillsTableView.isHidden = true
            skillsLabel.isHidden = true
        }

        profilePhoto.isUserInteractionEnabled = true
        profilePhoto.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressOnImage)))
        projectsTableView.layer.cornerRadius = 10
        skillsTableView.layer.cornerRadius = 10

        coalitionBackground.layer.cornerRadius = 10
        coalitionBackground.layer.masksToBounds = true
        self.title = userToShow?.login
        if (userToShow?.imageUrl != nil) {
            let group = DispatchGroup()
            group.enter()
            getImageFromUrl(urlText: (userToShow?.imageUrl)!) {
                group.leave()
            }
         
            group.wait()
       }
        self.profilePhoto.image = self.imageFromUrl
        if (userToShow?.coalitionInfo == nil) || (userToShow?.coalitionInfo?.id == nil) || (coalBackgrounds[(userToShow?.coalitionInfo?.id)!] == nil) {
            self.coalitionBackground.image = UIImage(named: "school_42_background")
        }
        else {
            let group2 = DispatchGroup()
            group2.enter()
            getImageFromUrl(urlText: coalBackgrounds[(userToShow?.coalitionInfo?.id)!]!) {
                group2.leave()
                DispatchQueue.main.async {
                    self.coalitionBackground.image = self.imageFromUrl
                }
                group2.wait()

        }
        }

        studentNameLabel.text = userToShow?.displayName
        emailLabel.text = "📨\(userToShow?.email ?? "No info")"
        emailLabel.isUserInteractionEnabled = true
        emailLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressOnEmail)))
        mobileLabel.text = "📱 \(userToShow?.phone ?? "No info")"
        mobileLabel.isUserInteractionEnabled = true
        mobileLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressOnPhoneNumber)))
        if (userToShow?.cursusUsers != nil && userToShow?.cursusUsers![0].level != nil) {
            levelLabel.text = "Level \(String(describing: Int((userToShow?.cursusUsers![0].level)!)))"
        }
        locationLabel.text = "🖥 \(userToShow?.location ?? "Unavailable")"
        if (userToShow?.wallet != nil) {
            walletLabel.text = "Wallet: \(String(describing: (userToShow?.wallet)!))"
        }
        if (userToShow?.correctionPoints != nil) {
            correctionsLabel.text = "Corrections: \(String(describing: (userToShow?.correctionPoints)!))"
        }
        DispatchQueue.main.async {
            self.profilePhoto.layer.cornerRadius = self.profilePhoto.layer.frame.size.width / 2
            self.drowAnimatedLevelBar()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func drowAnimatedLevelBar() {
        
        var arcRadius = CGFloat()
        var arcCenter = CGPoint()
        arcRadius = profilePhoto.layer.frame.size.width / 2
        arcCenter = CGPoint(x: profilePhoto.center.x, y: profilePhoto.center.y)

        let trackPath = UIBezierPath(arcCenter: arcCenter, radius:arcRadius, startAngle: 2.094, endAngle: 1.047, clockwise: true)
        circleLayerTrack.path = trackPath.cgPath
        let trackColor = UIColor.lightGray.withAlphaComponent(0.55)
        circleLayerTrack.strokeColor = trackColor.cgColor
        circleLayerTrack.lineWidth = 9
        circleLayerTrack.fillColor = UIColor.clear.cgColor
        circleLayerTrack.lineCap = kCALineCapRound
        view.layer.addSublayer(circleLayerTrack)

        var levelPercentage: Int
        if (userToShow?.cursusUsers != nil && userToShow?.cursusUsers![0].level != nil) {
            var doubleValue = (userToShow?.cursusUsers![0].level!)!
            doubleValue = doubleValue.truncatingRemainder(dividingBy: 1) * 100
            levelPercentage = Int(floor(doubleValue))
            levelPercentage += levelPercentage > 0 ? 1 : 0
        }
        else {
            levelPercentage = 100
        }
        self.levelPercentage?.text = "\(String(describing: levelPercentage))%"
//        print(levelPercentage)
//        print("percentage: \(levelPercentage)")

        let scaleEnd = (300 * levelPercentage) / 100
//        print("scacleEnd: \(scaleEnd)")
        self.progressBarEndAngle = CGFloat(Measurement(value: Double(scaleEnd), unit: UnitAngle.degrees)
            .converted(to: .radians).value) + 2.094
        
        let circlePath = UIBezierPath(arcCenter: arcCenter, radius: arcRadius, startAngle: 2.094, endAngle: progressBarEndAngle, clockwise: true)
        circleLayer.path = circlePath.cgPath
        circleLayer.strokeColor = UIColor.yellow.cgColor
        circleLayer.lineWidth = 7
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineCap = kCALineCapRound
        circleLayer.strokeEnd = 0
        view.layer.addSublayer(circleLayer)

    }
    func perfomAnimation() {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = 1
        animation.duration = 3.2
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        circleLayer.add(animation, forKey: "animation")
    }
    
    override func viewWillTransition( to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator ) {
        DispatchQueue.main.async() {
            let newCirclePath = UIBezierPath(arcCenter: CGPoint(x: self.profilePhoto.center.x, y: self.profilePhoto.center.y), radius: self.profilePhoto.layer.frame.size.width / 2, startAngle: 2.094, endAngle: self.progressBarEndAngle, clockwise: true)
            self.circleLayer.path = newCirclePath.cgPath
            let newTrackPath = UIBezierPath(arcCenter: CGPoint(x: self.profilePhoto.center.x, y: self.profilePhoto.center.y), radius: self.profilePhoto.layer.frame.size.width / 2, startAngle: 2.094, endAngle: 1.047, clockwise: true)
            self.circleLayerTrack.path = newTrackPath.cgPath
            self.profilePhoto.layer.cornerRadius = self.profilePhoto.layer.frame.size.width / 2
        }

        
    }
    
    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var coalitionBackground: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelPercentage: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var walletLabel: UILabel!
    @IBOutlet weak var correctionsLabel: UILabel!
    @IBOutlet weak var projectsTableView: UITableView!
    @IBOutlet weak var skillsTableView: UITableView!
    @IBOutlet weak var projectsLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
}
