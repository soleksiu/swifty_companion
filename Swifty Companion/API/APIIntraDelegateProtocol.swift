//
//  APIIntraDelegateProtocol.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/12/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import Foundation

protocol APIIntraDelegate : class {
    func receiveUserInfo(info: User)
    func errorHandler(error: NSError)
    func showErrorMessage(titleText: String, messageText: String, buttonText: String)
}
