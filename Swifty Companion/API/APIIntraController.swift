//
//  APIIntraController.swift
//  Swifty Companion
//
//  Created by Sergii OLEKSIUK on 2/12/19.
//  Copyright © 2019 Sergii OLEKSIUK. All rights reserved.
//

import Foundation
import UIKit

class APIIntraController {
    weak var delegate: APIIntraDelegate?
    let accessToken: String?
    var userInfo: User?
    
    init(delegate: APIIntraDelegate, tokenToSet: String) {
        self.delegate = delegate
        self.accessToken = tokenToSet
    }
    
    func getCoalitionInfo(requestBody: String, completion: @escaping () -> ()) {
        //                    GET /v2/users/:user_id/coalitions
        let urlGetCoalitionInfo = "https://api.intra.42.fr/v2/users/" + requestBody + "/coalitions"
        //_____________________getting coalition data___________//
        var requestCoalitiion: NSMutableURLRequest?
        // TODO: show error message if no results from search request, or search words is not correct
        if let urlCoalitionForAPI = NSURL(string: urlGetCoalitionInfo) {
            requestCoalitiion = NSMutableURLRequest(url: urlCoalitionForAPI as URL)
            requestCoalitiion?.httpMethod = "GET"
            requestCoalitiion?.setValue("Bearer " + accessToken!, forHTTPHeaderField: "Authorization")
        }
        else {
            return
        }
        let taskGetCoalition = URLSession.shared.dataTask(with: requestCoalitiion! as URLRequest)
        {
            
            (data, response, error) in
            //                        print(response ?? "")
            if error != nil {
                self.delegate?.errorHandler(error: error! as NSError)
            }
            else if let coalitionData = data {
                do {
//                                                    let dic  = try JSONSerialization.jsonObject(with: coalitionData, options: JSONSerialization.ReadingOptions.mutableContainers)
//                                                    print(dic)
                    let coalitionInfo = try JSONDecoder().decode([Coalition].self, from: coalitionData)
//                    print(coalitionInfo)
                    self.userInfo?.coalitionInfo = coalitionInfo[0]


                }
                    
                catch (let err) {
                    self.delegate?.errorHandler(error: err as NSError)
                }
            }
            completion()
        }
        taskGetCoalition.resume()

    }
    
    func makeIntraRequest(indicator: UIActivityIndicatorView, requestBody: String, completion: @escaping () -> ()) {
        //        curl  -H "Authorization: Bearer YOUR_AvarSS_TOKEN" "https://api.intra.42.fr/v2/users/2"
        let urlGetUserInfo = "https://api.intra.42.fr/v2/users/" + requestBody
        //accessToken: 359453c09ba536587bda3824005510450b0f69415aec55090dac878823a42395

        var request: NSMutableURLRequest?
        // TODO: show error message if no results from search request, or search words is not correct
        if let urlForAPI = NSURL(string: urlGetUserInfo) {
            request = NSMutableURLRequest(url: urlForAPI as URL)
            request?.httpMethod = "GET"
            request?.setValue("Bearer " + accessToken!, forHTTPHeaderField: "Authorization")
        }
        else {
            return
        }
        //        var curUser = User?.self
        //request.setValue("100", forHTTPHeaderField: "Count")
        //request.httpBody = "count=100".data(using: String.Encoding.utf8, allowLossyConversion: false)
        //print(request.allHTTPHeaderFields)
        let task = URLSession.shared.dataTask(with: request! as URLRequest)
        {
            
            (data, response, error) in
//                    print(response ?? "")
            DispatchQueue.main.async {
                indicator.startAnimating()
            }

            if error != nil {
                self.delegate?.errorHandler(error: error! as NSError)
            }
            else if let userData = data {
                do {
                    
                    
                    //_____________________data parsing using decodable___________//

                    self.userInfo = try JSONDecoder().decode(User.self, from: userData)
                    var elemtIndex = 0
                    if self.userInfo?.projectsUsers != nil {
                        for project in (self.userInfo?.projectsUsers!)! {
                            if (project.cursusIDS![0] == 4) || (project.project?.name?.contains("Day"))! || (project.project?.name?.contains("Rush"))! || (project.project?.name?.contains("Exam0"))! {
                                self.userInfo?.projectsUsers?.remove(at:elemtIndex)
                                elemtIndex -= 1
                                
                            }
                            elemtIndex += 1
                        }
                    }

                    //_____________________data parsing using decodable___________//
                    
                    //_____________________standart parsing: getting dictionary___________//
                    

//                                        let dic  = try JSONSerialization.jsonObject(with: userData, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String : Any]
//                                        print(dic)
                    //                    dump(dic)
//                                        self.delegate?.receiveUserInfo(info: userInfo)

                    //_____________________standart parsing: getting dictionary___________//

                    
                    //_____________________displaying parsed user info___________//
//                    print("********************************************************")
//                    print("Parsed user info:")
//                    print("Name:    ", userInfo.displayName ?? "Not found")
//                    print("Login:   ", userInfo.login ?? "Not found")
//                    print("Img URL: ", userInfo.imageUrl ?? "Not found")
//                    print("Location:", userInfo.location ?? "Not found")
//                    print("Email:   ", userInfo.email ?? "Not found")
//                    print("Phone:   ", userInfo.phone ?? "Not found")
//                    print("Level:   ", userInfo.cursusUsers![0].level ?? "Not found")
//                    print("Cr point:", userInfo.correctionPoints ?? "Not found")
//                    print("Wallet:  ", userInfo.wallet ?? "Not found")
//                    print("Grade:   ", userInfo.cursusUsers![0].grade ?? "Not found", "\n")
//                    print("********************************************************")
//
//                    //_____________________displaying projects array___________//
//                    print("Parsed projects info:")
//                    elemtIndex = 0
//                    var validatedStr = "No value"
//                    for pr in userInfo.projectsUsers! {
//                        print("Project:      ", pr.project!.name ?? "No value")
//                        print("    status:   ", pr.status ?? "No value")
//                        if pr.validated != nil {
//                            validatedStr = pr.validated! == 1 ? "Yes" : "No"
//                        }
//                        else {
//                            validatedStr = "No value"
//                        }
//                        print("    validated:", validatedStr)
//                        print("    mark:     ", pr.finalMark ?? "No value")
//                        print("___________________________________________")
//                        elemtIndex += 1
//                    }
//                    print("Overal amount of projects: \(elemtIndex)")
                    
                }
                catch (let err) {
                    
                    self.delegate?.errorHandler(error: err as NSError)
                    
                }
            }
            completion()


        }
        task.resume()
    }
}
