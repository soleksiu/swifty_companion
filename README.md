# Swifty Companion.
###  Language of development: Swift 4.0.
This is a learning project of Unit Factory coding school, which using
'ecole 42' franchise and same learning programm as the network of innovative
schools across the world in such countries as USA, France, Netherlands.

 The aim of the project is to build an application that will retrieve the
 information of 42 student, using the 42 school API. 
 ## Installation
Simply open the Swifty Companion.xcodeproj
or use the command from console:
```open Swifty\ Companion.xcodeproj```
## Usage
Input the login of the student and press search button.

## Interface screenshots

![Screenshot](Screenshots/launchscreen.png)![Screenshot](Screenshots/search_screen.png)![Screenshot](Screenshots/results_screen.png)![Screenshot](Screenshots/scroll_view.png)
### Actions when tapping on number or email
![Screenshot](Screenshots/make_a_call.png)![Screenshot](Screenshots/send_email.png)
### Errors handling
![Screenshot](Screenshots/error1.png)![Screenshot](Screenshots/error2.png)
